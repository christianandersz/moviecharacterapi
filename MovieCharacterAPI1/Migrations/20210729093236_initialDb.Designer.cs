﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using MovieCharacterAPI1.Models;

namespace MovieCharacterAPI1.Migrations
{
    [DbContext(typeof(MovieCharacterDbContext))]
    [Migration("20210729093236_initialDb")]
    partial class initialDb
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("ProductVersion", "5.0.8")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("MovieCharacterAPI1.Models.Domain.Character", b =>
                {
                    b.Property<int>("CharacterId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Alias")
                        .HasMaxLength(60)
                        .HasColumnType("nvarchar(60)");

                    b.Property<string>("FullName")
                        .IsRequired()
                        .HasMaxLength(120)
                        .HasColumnType("nvarchar(120)");

                    b.Property<string>("Gender")
                        .HasMaxLength(50)
                        .HasColumnType("nvarchar(50)");

                    b.Property<string>("PictureUrl")
                        .HasMaxLength(255)
                        .HasColumnType("nvarchar(255)");

                    b.HasKey("CharacterId");

                    b.ToTable("Character");

                    b.HasData(
                        new
                        {
                            CharacterId = 1,
                            Alias = "Crille",
                            FullName = "Christian",
                            Gender = "Male",
                            PictureUrl = "https://Somepictureonline.com/"
                        },
                        new
                        {
                            CharacterId = 2,
                            Alias = "Perra",
                            FullName = "Peter",
                            Gender = "Male",
                            PictureUrl = "https://Somepictureonline.com/"
                        });
                });

            modelBuilder.Entity("MovieCharacterAPI1.Models.Domain.Franchise", b =>
                {
                    b.Property<int>("FranchiseId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Description")
                        .IsRequired()
                        .HasMaxLength(255)
                        .HasColumnType("nvarchar(255)");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(60)
                        .HasColumnType("nvarchar(60)");

                    b.HasKey("FranchiseId");

                    b.ToTable("Franchise");

                    b.HasData(
                        new
                        {
                            FranchiseId = 1,
                            Description = "First franchise ever.",
                            Name = "First Franchise"
                        },
                        new
                        {
                            FranchiseId = 2,
                            Description = "Second franchise.",
                            Name = "Second Franchise"
                        });
                });

            modelBuilder.Entity("MovieCharacterAPI1.Models.Domain.Movie", b =>
                {
                    b.Property<int>("MovieId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Director")
                        .IsRequired()
                        .HasMaxLength(50)
                        .HasColumnType("nvarchar(50)");

                    b.Property<int?>("FranchiseId")
                        .HasColumnType("int");

                    b.Property<string>("Genre")
                        .IsRequired()
                        .HasMaxLength(50)
                        .HasColumnType("nvarchar(50)");

                    b.Property<string>("PictureUrl")
                        .HasMaxLength(255)
                        .HasColumnType("nvarchar(255)");

                    b.Property<int>("ReleaseYear")
                        .HasColumnType("int");

                    b.Property<string>("Title")
                        .IsRequired()
                        .HasMaxLength(50)
                        .HasColumnType("nvarchar(50)");

                    b.Property<string>("TrailerUrl")
                        .HasMaxLength(255)
                        .HasColumnType("nvarchar(255)");

                    b.HasKey("MovieId");

                    b.HasIndex("FranchiseId");

                    b.ToTable("Movie");

                    b.HasData(
                        new
                        {
                            MovieId = 1,
                            Director = "Posty",
                            FranchiseId = 1,
                            Genre = "Action",
                            PictureUrl = "Pic pic",
                            ReleaseYear = 1996,
                            Title = "Some sick movie",
                            TrailerUrl = "https://youtube.com/"
                        },
                        new
                        {
                            MovieId = 2,
                            Director = "Posty",
                            FranchiseId = 1,
                            Genre = "Action",
                            PictureUrl = "Pic pic",
                            ReleaseYear = 2001,
                            Title = "Some sick movie 2",
                            TrailerUrl = "https://youtube.com/"
                        });
                });

            modelBuilder.Entity("MovieCharacterAPI1.Models.Domain.MovieCharacter", b =>
                {
                    b.Property<int>("MovieId")
                        .HasColumnType("int");

                    b.Property<int>("CharacterId")
                        .HasColumnType("int");

                    b.HasKey("MovieId", "CharacterId");

                    b.HasIndex("CharacterId");

                    b.ToTable("MovieCharacter");
                });

            modelBuilder.Entity("MovieCharacterAPI1.Models.Domain.Movie", b =>
                {
                    b.HasOne("MovieCharacterAPI1.Models.Domain.Franchise", "Franchise")
                        .WithMany("Movies")
                        .HasForeignKey("FranchiseId");

                    b.Navigation("Franchise");
                });

            modelBuilder.Entity("MovieCharacterAPI1.Models.Domain.MovieCharacter", b =>
                {
                    b.HasOne("MovieCharacterAPI1.Models.Domain.Character", "Character")
                        .WithMany("Movies")
                        .HasForeignKey("CharacterId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("MovieCharacterAPI1.Models.Domain.Movie", "Movie")
                        .WithMany("Characters")
                        .HasForeignKey("MovieId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Character");

                    b.Navigation("Movie");
                });

            modelBuilder.Entity("MovieCharacterAPI1.Models.Domain.Character", b =>
                {
                    b.Navigation("Movies");
                });

            modelBuilder.Entity("MovieCharacterAPI1.Models.Domain.Franchise", b =>
                {
                    b.Navigation("Movies");
                });

            modelBuilder.Entity("MovieCharacterAPI1.Models.Domain.Movie", b =>
                {
                    b.Navigation("Characters");
                });
#pragma warning restore 612, 618
        }
    }
}
