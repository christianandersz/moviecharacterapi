﻿using Microsoft.EntityFrameworkCore;
using MovieCharacterAPI1.Models.Domain;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacterAPI1.Models
{
    public class MovieCharacterDbContext : DbContext
    {
        // Tables
        public DbSet<Character> Characters { get; set; }
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Franchise> Franchises { get; set; }
        public DbSet<MovieCharacter> MovieCharacter { get; set; }

        // Constructor
        public MovieCharacterDbContext([NotNullAttribute] DbContextOptions options) : base(options)
        {

        }

        // Seeding
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Seed data
            modelBuilder.Entity<Character>().HasData(
                new Character
                {
                    CharacterId = 1,
                    FullName = "Christian",
                    Alias = "Crille",
                    Gender = "Male",
                    PictureUrl = "https://Somepictureonline.com/"
                },
                new Character
                {
                    CharacterId = 2,
                    FullName = "Peter",
                    Alias = "Perra",
                    Gender = "Male",
                    PictureUrl = "https://Somepictureonline.com/"
                });

            modelBuilder.Entity<Movie>().HasData(
                new Movie
                {
                    MovieId = 1,
                    Title = "Some sick movie",
                    Genre = "Action",
                    Director = "Posty",
                    PictureUrl = "Pic pic",
                    TrailerUrl = "https://youtube.com/",
                    ReleaseYear = 1996,
                    FranchiseId = 1
                },
                new Movie
                {
                    MovieId = 2,
                    Title = "Some sick movie 2",
                    Genre = "Action",
                    Director = "Posty",
                    PictureUrl = "Pic pic",
                    TrailerUrl = "https://youtube.com/",
                    ReleaseYear = 2001,
                    FranchiseId = 1,
                });

            modelBuilder.Entity<Franchise>().HasData(
                new Franchise
                {
                    FranchiseId = 1,
                    Name = "First Franchise",
                    Description = "First franchise ever."
                },
                new Franchise
                {
                    FranchiseId = 2,
                    Name = "Second Franchise",
                    Description = "Second franchise."
                });

            // Set up M2M MovieCharacter.
            modelBuilder.Entity<MovieCharacter>().HasKey(mc => new { mc.MovieId, mc.CharacterId });
            // Set up O2M franchise-movies.
            modelBuilder.Entity<Movie>()
                .HasOne(m => m.Franchise)
                .WithMany(f => f.Movies)
                .HasForeignKey(m => m.FranchiseId);
        }
    }
}
