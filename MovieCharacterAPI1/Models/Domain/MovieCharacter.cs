﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacterAPI1.Models.Domain
{
    [Table("MovieCharacter")]
    public class MovieCharacter
    {
        public int CharacterId { get; set; }
        public Character Character { get; set; }

        public int MovieId { get; set; }
        public Movie Movie { get; set; }
    }
}
