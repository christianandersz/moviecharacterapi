﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacterAPI1.Models.Domain
{
    [Table("Movie")]
    public class Movie
    {
        // Primary Key
        public int MovieId { get; set; }

        // Fields
        [Required]
        [MaxLength(50)]
        public string Title { get; set; }

        [Required]
        [MaxLength(50)]
        public string Genre { get; set; }

        [Required]
        public int ReleaseYear { get; set; }

        [Required]
        [MaxLength(50)]
        public string Director { get; set; }

        [MaxLength(255)]
        public string PictureUrl { get; set; }

        [MaxLength(255)]
        public string TrailerUrl { get; set; }

        // Relationships
        public virtual ICollection<MovieCharacter> Characters { get; set; }
        public int? FranchiseId { get; set; }
        public Franchise Franchise { get; set; }
    }
}
