﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacterAPI1.Models.Domain
{
    [Table("Character")]
    public class Character
    {
        // Primary key
        public int CharacterId { get; set; }

        // Fields
        [Required]
        [MaxLength(120)]
        public string FullName { get; set; }
        [MaxLength(60)]
        public string Alias { get; set; }
        [MaxLength(50)]
        public string Gender { get; set; }
        [MaxLength(255)]
        public string PictureUrl { get; set; }

        // Relationships
        public virtual ICollection<MovieCharacter> Movies { get; set; }
    }
}
