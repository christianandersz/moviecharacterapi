﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacterAPI1.Models.Domain
{
    [Table("Franchise")]
    public class Franchise
    {
        // Primary Key
        public int FranchiseId { get; set; }

        // Fields 
        [Required]
        [MaxLength(60)]
        public string Name { get; set; }

        [Required]
        [MaxLength(255)]
        public string Description { get; set; }

        // Relationships

        public virtual ICollection<Movie> Movies { get; set; }
    }
}
