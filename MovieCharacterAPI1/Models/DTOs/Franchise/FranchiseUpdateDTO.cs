﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacterAPI1.Models.DTOs.Franchise
{
    public class FranchiseUpdateDTO
    {
        public int FranchiseId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public int? MovieId { get; set; }
    }
}
