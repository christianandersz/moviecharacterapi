﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacterAPI1.Models.DTOs.Character
{
    public class CharacterReadDTO
    {
        public int CharacterId { get; set; }

        public string FullName { get; set; }
        public string Alias { get; set; }
        public string Gender { get; set; }
        public string PictureUrl { get; set; }

        public List<int> Movies { get; set; }
    }
}
