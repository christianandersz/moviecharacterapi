﻿using AutoMapper;
using MovieCharacterAPI1.Models.Domain;
using MovieCharacterAPI1.Models.DTOs.Franchise;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacterAPI1.Profiles
{
    public class FranchiseProfile : Profile
    {
        public FranchiseProfile()
        {
            // Franchise <-> FranchiseReadDTO
            CreateMap<Franchise, FranchiseReadDTO>().ReverseMap();

            // Franchise <-> FranchiseCreateDTO
            CreateMap<Franchise, FranchiseCreateDTO>().ReverseMap();

            // Franchise <-> FranchiseUpdateDTO
            CreateMap<Franchise, FranchiseUpdateDTO>().ReverseMap();
        }
    }
}
