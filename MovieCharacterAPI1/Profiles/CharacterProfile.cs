﻿using AutoMapper;
using MovieCharacterAPI1.Models.Domain;
using MovieCharacterAPI1.Models.DTOs.Character;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacterAPI1.Profiles
{
    public class CharacterProfile : Profile
    {
        public CharacterProfile()
        {
            // Character <-> CharacterReadDTO
            CreateMap<Character, CharacterReadDTO>()
                .ForMember(cdto => cdto.Movies, opt => opt
                .MapFrom(c => c.Movies.Select(m => m.MovieId).ToList())).ReverseMap();

            // Character <-> CharacterCreateDTO
            CreateMap<Character, CharacterCreateDTO>().ReverseMap();

            // Character <-> CharacterUpdateDTO
            CreateMap<Character, CharacterUpdateDTO>().ReverseMap();
        }
    }
}
