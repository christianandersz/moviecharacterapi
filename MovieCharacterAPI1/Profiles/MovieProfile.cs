﻿using AutoMapper;
using MovieCharacterAPI1.Models.Domain;
using MovieCharacterAPI1.Models.DTOs.Movie;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacterAPI1.Profiles
{
    public class MovieProfile : Profile
    {
        public MovieProfile()
        {
            // Movie <-> MovieReadDTO
            CreateMap<Movie, MovieReadDTO>()
                .ForMember(mdto => mdto.Characters, opt => opt
                .MapFrom(m => m.Characters
                .Select(c => c.CharacterId).ToArray()))
                .ForMember(mdto => mdto.Franchise, opt => opt
                .MapFrom(m => m.FranchiseId))
                .ReverseMap();

            // Movie <-> MovieCreateDTO
            CreateMap<Movie, MovieCreateDTO>().ReverseMap();

            // Movie <-> MovieUpdateDTO
            CreateMap<Movie, MovieUpdateDTO>().ReverseMap();
        }
    }
}
