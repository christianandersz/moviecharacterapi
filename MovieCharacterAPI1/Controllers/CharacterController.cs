﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharacterAPI1.Models;
using MovieCharacterAPI1.Models.Domain;
using MovieCharacterAPI1.Models.DTOs.Character;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;

namespace MovieCharacterAPI1.Controllers
{
    [Route("api/v1/characters")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class CharacterController : ControllerBase
    {
        private readonly MovieCharacterDbContext _context;
        private readonly IMapper _mapper;

        public CharacterController(MovieCharacterDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }


        /// <summary>
        /// Get all Characters
        /// </summary>
        /// <returns>Returns a list of all the characters</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetAll()
        {
            var characters = await _context.Characters.Include(m => m.Movies).ToListAsync();
            List<CharacterReadDTO> charRead = _mapper.Map<List<CharacterReadDTO>>(characters);
            return charRead;
        }

        /// <summary>
        /// Get specific character by id.
        /// </summary>
        /// <param name="characterId">Id for the character that you want to get.</param>
        /// <returns>Returns teh chosen character.</returns>
        [HttpGet("{characterId}")]
        public async Task<ActionResult<CharacterReadDTO>> GetById(int characterId)
        {
            var character = await _context.Characters.FindAsync(characterId);
            if (character == null)
            {
                return NotFound();
            }

            return _mapper.Map<CharacterReadDTO>(character);
        }

        /// <summary>
        /// Adds new character to database.
        /// </summary>
        /// <param name="character">Has all the needed properties to create a character.</param>
        /// <exception cref="DbUpdateConcurrencyException">Throws if it fails to post new character.</exception>
        /// <returns>Adds a new character to database.</returns>
        [HttpPost]
        public async Task<ActionResult<CharacterReadDTO>> Post([FromBody] CharacterCreateDTO character)
        {
            Character domainCharacter = _mapper.Map<Character>(character);

            try
            {
                _context.Characters.Add(domainCharacter);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }

            // Maps the new character and gets it by Id.
            CharacterReadDTO newCharacter = _mapper.Map<CharacterReadDTO>(domainCharacter);

            return CreatedAtAction("GetById", new { Id = newCharacter.CharacterId }, newCharacter);
        }

        /// <summary>
        /// Update a character. 
        /// </summary>
        /// <param name="characterId">Id for the character that you want to update.</param>
        /// <param name="updatedCharacter">Has all the needed properties to update a character.</param>
        /// <exception cref="DbUpdateConcurrencyException">Throws if it fails to update chosen character.</exception>
        /// <returns>Updates the chosen character in the database.</returns>
        [HttpPut("{characterId}")]
        public async Task<ActionResult> Put(int characterId, CharacterUpdateDTO updatedCharacter)
        {
            if (characterId != updatedCharacter.CharacterId)
            {
                return BadRequest();
            }

            Character domainCharacter = _mapper.Map<Character>(updatedCharacter);

            try
            {
                _context.Entry(domainCharacter).State = EntityState.Modified;
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {

                throw;
            }
            return NoContent();
        }

        /// <summary>
        /// Delete a character.
        /// </summary>
        /// <param name="characterId">Id for the character you want to delete.</param>
        /// <exception cref="DbUpdateConcurrencyException">Throws if it fails to delete the chosen character from the database.</exception>
        /// <returns>Deletes the chosen character.</returns>
        [HttpDelete("{characterId}")]
        public async Task<ActionResult> Delete(int characterId)
        {
            var character = await _context.Characters.FindAsync(characterId);

            if (character == null)
            {
                return NotFound();
            }

            try
            {
                _context.Characters.Remove(character);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {

                throw;
            }
            
            return NoContent();
        }

        /// <summary>
        /// Adds a character to a movie.
        /// </summary>
        /// <param name="characterId">Id for the character that you want to add.</param>
        /// <param name="movieId">Id for the movie that you want to add.</param>
        /// <exception cref="DbUpdateConcurrencyException">Throws if it fails to add character to movie.</exception>
        /// <returns>Adds the chosen characterId and movieId together.</returns>
        [HttpPost("/MovieCharacter")]
        public async Task<IActionResult> AddCharacterToMovie(int characterId, int movieId)
        {

            MovieCharacter movieChar = new()
            {
                MovieId = movieId,
                CharacterId = characterId
            };

            if (movieChar == null || movieId == 0)
            {
                return NotFound();
            }

            try
            {
                _context.MovieCharacter.Add(movieChar);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }

            return NoContent();
        }

        /// <summary>
        /// Removes a character from a movie.
        /// </summary>
        /// <param name="characterId">Id for the character that you want to remove.</param>
        /// <param name="movieId">Id for the movie that you want to remove the character from.</param>
        /// <exception cref="DbUpdateConcurrencyException">Throws if it fails to remove character from movie. </exception>
        /// <returns>Removes the chosen character for the chosen movie.</returns>
        [HttpDelete("/MovieCharacter")]
        public async Task<IActionResult> RemoveCharacterFromMovie(int characterId, int movieId)
        {
            var removeChar = await _context.MovieCharacter.FirstOrDefaultAsync(c => c.CharacterId == characterId && c.MovieId == movieId);

            if (removeChar == null)
            {
                return NotFound();
            }

            try
            {
                var mc = _context.MovieCharacter.Remove(removeChar);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }

            return NoContent();
        }

    }
}
