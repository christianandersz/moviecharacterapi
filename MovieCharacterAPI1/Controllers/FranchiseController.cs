﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharacterAPI1.Models;
using MovieCharacterAPI1.Models.Domain;
using MovieCharacterAPI1.Models.DTOs.Character;
using MovieCharacterAPI1.Models.DTOs.Franchise;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;

namespace MovieCharacterAPI1.Controllers
{
    [Route("api/v1/franchises")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class FranchiseController : ControllerBase
    {
        private readonly MovieCharacterDbContext _context;
        private readonly IMapper _mapper;

        public FranchiseController(MovieCharacterDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Get all franchises
        /// </summary>
        /// <returns>Returns a list of all the franchises</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FranchiseReadDTO>>> GetAll()
        {
            var franchises = await _context.Franchises.ToListAsync();

            List<FranchiseReadDTO> franchiseRead = _mapper.Map<List<FranchiseReadDTO>>(franchises);

            return franchiseRead;
        }

        /// <summary>
        /// Get specific franchise by id.
        /// </summary>
        /// <param name="franchiseId">Id for the franchise that you want to get.</param>
        /// <returns>Returns the chosen franchise.</returns>
        [HttpGet("{franchiseId}")]
        public async Task<ActionResult<FranchiseReadDTO>> GetById(int franchiseId)
        {

            Franchise domainFranchise = await _context.Franchises.FindAsync(franchiseId);

            if (domainFranchise == null)
            {
                return NotFound();
            }

            return _mapper.Map<FranchiseReadDTO>(domainFranchise);
        }

        /// <summary>
        /// Adds a new franchise to the database.
        /// </summary>
        /// <param name="franchise">Has all the needed properties to create a franchise.</param>
        /// <exception cref="DbUpdateConcurrencyException">Throws if it fails to post new fanchise.</exception>
        /// <returns>Adds a new franchise to the database.</returns>
        [HttpPost]
        public async Task<ActionResult<Franchise>> Post(FranchiseCreateDTO franchise)
        {
            Franchise domainFranchise = _mapper.Map<Franchise>(franchise);

            try
            {
                _context.Franchises.Add(domainFranchise);
                await _context.SaveChangesAsync();
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

            // Maps the new franchise and gets it by Id.
            FranchiseReadDTO newFranchise = _mapper.Map<FranchiseReadDTO>(domainFranchise);

            return CreatedAtAction("GetById", new { Id = newFranchise.FranchiseId }, _mapper.Map<FranchiseReadDTO>(domainFranchise));
        }

        /// <summary>
        /// Update a franchise. 
        /// </summary>
        /// <param name="franchiseId">Id for the franchise that you want to update.</param>
        /// <param name="updatedFranchise">Has all the needed properties to update a franchise.</param>
        /// <exception cref="DbUpdateConcurrencyException">Throws if it fails to update franchise.</exception>
        /// <returns>Updates the chosen franchise in the database.</returns>
        [HttpPut("{franchiseId}")]
        public async Task<ActionResult> Put(int franchiseId, FranchiseUpdateDTO updatedFranchise)
        {
            if (franchiseId != updatedFranchise.FranchiseId)
            {
                return BadRequest();
            }

            Franchise domainFranchise = _mapper.Map<Franchise>(updatedFranchise);

            try
            {
                _context.Entry(domainFranchise).State = EntityState.Modified;
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
            
            return NoContent();

        }


        /// <summary>
        /// Delete a franchise.
        /// </summary>
        /// <param name="franchiseId">Id for the franchise that you want to delete.</param>
        /// <exception cref="DbUpdateConcurrencyException">Throws if it fails to delete the chosen franchise from the database.</exception>
        /// <returns>Deletes the chosen franchise from the database..</returns>
        [HttpDelete("{franchiseId}")]
        public async Task<IActionResult> Delete(int franchiseId)
        {
            var franchise = await _context.Franchises.FindAsync(franchiseId);

            if (franchise == null)
            {
                return NotFound();
            }

            try
            {
                _context.Franchises.Remove(franchise);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }

            return NoContent();
        }


        /// <summary>
        /// Adds a movie to a franchise.
        /// </summary>
        /// <param name="franchiseId">Id for the franchise that you want to chose.</param>
        /// <param name="moviesId">Id for the movie that you want to add.</param>
        /// <exception cref="DbUpdateConcurrencyException">Throws if it fails to add movie to franchise.</exception>
        /// <returns>Adds the chosen movieId and franchiseId together.</returns>
        [HttpPatch("{franchiseId}/movies")]
        public async Task<IActionResult> AddMovieToFranchise(int franchiseId, int moviesId )
        {
            Franchise franchise = await _context.Franchises.FindAsync(franchiseId);
            Movie movie = await _context.Movies.FindAsync(moviesId);
            if (franchise == null || movie == null)
            {
                return NotFound();
            }

            try
            {
                movie.FranchiseId = franchise.FranchiseId;
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
            return NoContent();
        }


        /// <summary>
        /// Removes movie from a franchise.
        /// </summary>
        /// <param name="franchiseId">Id for the franchise.</param>
        /// <param name="movieId">Id for the movie that you want to remove.</param>
        /// <exception cref="DbUpdateConcurrencyException">Throws if it fails to remove chosen movie from chosen franchise.</exception>
        /// <returns>Removes the chosen movie from the chosen franchise.</returns>
        [HttpPatch("{franchiseId}")]
        public async Task<IActionResult> DeleteMovieFromFranchise(int franchiseId, int movieId)
        {
            var movie = await _context.Movies.FirstOrDefaultAsync(c => c.MovieId == movieId && c.FranchiseId == franchiseId);

            if (movie == null)
            {
                return NotFound();
            }
            
            try
            {
                movie.FranchiseId = null;
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }

            return NoContent();
        }


        /// <summary>
        /// Get all characters in a franchise
        /// </summary>
        /// <param name="franchiseId">Id for the franchise that you want to get all characters from.</param>
        /// <returns>Returns a json of all the characters that exists in chosen franchise.</returns>
        [HttpGet("{franchiseId}/GetAllCharactersInFranchise")]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetAllCharactersInFranchise(int franchiseId)
        {
            var franchise = await _context.Franchises.FindAsync(franchiseId);
            if (franchise == null)
                return NotFound();
            FranchiseReadDTO domainFranchise = _mapper.Map<FranchiseReadDTO>(franchise);
            var charactersInFranchise = (from character in _context.Characters
                                         from movieChar in _context.MovieCharacter
                                         from movie in _context.Movies
                                         where character.CharacterId == movieChar.CharacterId
                                         where movieChar.MovieId == movie.MovieId
                                         where movie.FranchiseId == domainFranchise.FranchiseId
                                         select character).Distinct();
            if (charactersInFranchise.Count() == 0)
                return NotFound();
            return _mapper.Map<List<CharacterReadDTO>>(await charactersInFranchise.ToListAsync());
        }


    }
}
