﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharacterAPI1.Models;
using MovieCharacterAPI1.Models.Domain;
using MovieCharacterAPI1.Models.DTOs.Movie;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;

namespace MovieCharacterAPI1.Controllers
{
    [Route("api/v1/movies")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))] 

    public class MovieController : ControllerBase
    {
        private readonly MovieCharacterDbContext _context;
        private readonly IMapper _mapper;

        public MovieController(MovieCharacterDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }


        /// <summary>
        /// Get all Characters
        /// </summary>
        /// <returns>Returns a list of all the characters</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetAll()
        {
            var movies = await _context.Movies.Include(c => c.Characters).ToListAsync();

            List<MovieReadDTO> movieRead = _mapper.Map<List<MovieReadDTO>>(movies);

            return movieRead;
        }

        /// <summary>
        /// Get specific movie by id.
        /// </summary>
        /// <param name="movieId">Id for the movie that you want to get.</param>
        /// <returns>Returns the chosen movie.</returns>
        [HttpGet("{movieId}")]
        public async Task<ActionResult<MovieReadDTO>> GetById(int movieId)
        {

            var domainMovie = await _context.Movies.FindAsync(movieId);

            if (domainMovie == null)
            {
                return NotFound();
            }

            return _mapper.Map<MovieReadDTO>(domainMovie);
        }

        /// <summary>
        /// Adds a new movie to the database.
        /// </summary>
        /// <param name="movie">Has all the needed properties to create a movie.</param>
        /// <exception cref="DbUpdateConcurrencyException">Throws if it fails to post new movie.</exception>
        /// <returns>Adds a new movie to the database.</returns>
        [HttpPost]
        public async Task<ActionResult<Movie>> Post(MovieCreateDTO movie)
        {
            Movie domainMovie = _mapper.Map<Movie>(movie);

            try
            {
                _context.Movies.Add(domainMovie);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }

            // Maps the new movie and gets it by Id.
            MovieReadDTO newMovie = _mapper.Map<MovieReadDTO>(domainMovie);

            return CreatedAtAction("GetById", new { Id = newMovie.MovieId }, _mapper.Map<MovieReadDTO>(domainMovie));
        }

        /// <summary>
        /// Update a movie.
        /// </summary>
        /// <param name="movieId">Id for the movie that you want to update.</param>
        /// <param name="updatedMovie">Has all the properties to update movie.</param>
        /// <exception cref="DbUpdateConcurrencyException">Throws if it fails to update movie.</exception>
        /// <returns>Updates chosen movie in the database.</returns>
        [HttpPut("{movieId}")]
        public async Task<ActionResult> Put(int movieId, MovieUpdateDTO updatedMovie)
        {
            if (movieId != updatedMovie.MovieId)
            {
                return BadRequest();
            }

            Movie domainMovie = _mapper.Map<Movie>(updatedMovie);

            try
            {
                _context.Entry(domainMovie).State = EntityState.Modified;
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }

            return NoContent();

        }

        /// <summary>
        /// Delete a movie.
        /// </summary>
        /// <param name="movieId">Id of the movie that you want to delete.</param>
        /// <exception cref="DbUpdateConcurrencyException">Throws if it fails to delete the chosen movie from the database.</exception>
        /// <returns>Deletes the chosen movie from the database.</returns>
        [HttpDelete("{movieId}")]
        public async Task<ActionResult> Delete(int movieId)
        {
            var movie = await _context.Movies.FindAsync(movieId);

            if (movie == null)
            {
                return NotFound();
            }

            try
            {
                _context.Movies.Remove(movie);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }

            return NoContent();
        }
    }
}
